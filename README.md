Exercise for the Pickle Robot company demonstrating basic code ability and use of version control. 

The phone_wordifier.py contains the following 3 function
* number_to_words(numerical_phone_number)
* words_to_number(wordified_phone_number)
* all_wordification(numerical_phone_number)

The format of the numerical_phone_number must maintain the format of:
    
    x-xxx-xxx-xxxx #x representing a digit 

The format of the wordified_phone_number input must maint the format of:
 
    x-xxx-ooo-oooo # x's are digits, while o may be digits of letters
    
Testing can be done through PyTest:

Installing PyTest:

    pip install pytest

Running the tests (in the repository folder):

    pytest 