import random
from string import ascii_letters, digits
from itertools import product

numpad = {"1": "1", "2": "ABC", "3": "DEF", "4": "GHI", "5": "JKL", "6": "MNO", "7": "PQRS", "8": "TUV", "9": "WXYZ",
          "0": "0"}


def valid_phone_number(phone_number):
    # Sanitation for full number conversion
    if not isinstance(phone_number, str):
        print("Please provide a string!")
        return None
    if len(phone_number) == 0:
        print("No phone number has been provided!")
        return None
    split_phone_number = phone_number.split("-")
    if len(split_phone_number) == 4:
        try:
            if len(split_phone_number[0]) != 1:
                print("The country area code must be 1 digits long")
                return None
            if len(split_phone_number[1]) != 3:
                print("The area code must be 3 digits long")
                return None
            if len(split_phone_number[2]) != 3:
                print("The prefix must be 3 digits long")
                return None
            if len(split_phone_number[3]) != 4:
                print("The line number must be 4 digits long")
                return None

            [int(part) for part in split_phone_number]
            return True

        except ValueError:
            print("Please provide number only phone number")
            return None
    else:
        print("Phone number format is invalid\n Please provide phone number as 1-XXX-XXX-XXX")
        return None


def valid_wordified_phone_number(phone_number):
    # Sanitation for full number conversion
    if not isinstance(phone_number, str):
        print("Please provide a string!")
        return None
    if len(phone_number) == 0:
        print("No phone number has been provided!")
        return None

    split_phone_number = phone_number.split("-")
    if len(split_phone_number) == 3:
        try:
            if len(split_phone_number[1]) != 3:
                print("The area code must be 3 digits long")
                return None

            if len(split_phone_number[2]) != 7:
                print("The word in the provided phone number is not of the right length")
                return None

            for letter in split_phone_number[2]:
                if letter not in ascii_letters and letter not in digits:
                    print("Wordified phone number has invalid characters")
                    return None

            return True

        except ValueError:
            print("Please provide number only phone number")
            return None
    else:
        print("Phone number format is invalid\n Please provide phone number as 1-XXX-XXX-XXXX")
        return None


def number_to_words(phone_number):
    if valid_phone_number(phone_number):
        split_phone_number = phone_number.split("-")
        number = "".join(split_phone_number[2:])
        area_code = split_phone_number[:2]
        word = ""
        for digit in number:
            word += random.choice(numpad[digit])

        return "-".join(area_code + [word])


def words_to_number(phone_number):
    if valid_wordified_phone_number(phone_number):
        split_phone_number = phone_number.split("-")
        word = "".join(split_phone_number[2:])
        area_code = split_phone_number[:2]
        number = ""
        for letter in word:
            for digit, val in numpad.items():
                if letter in val or letter.upper() in val:
                    number += digit
                    break
            if len(number) == 3:
                number += "-"

        return "-".join(area_code + [number])


def all_wordification(phone_number):
    if valid_phone_number(phone_number):
        combinations = []

        split_phone_number = phone_number.split("-")
        number = "".join(split_phone_number[2:])
        area_code = split_phone_number[:2]
        word = []
        for digit in number:
            word += [numpad[digit] + digit]

        raw_combinations = product(*word)
        for i in list(raw_combinations):
            prefix_and_line_number = ["".join(list(i))]
            combinations.append("-".join(area_code + prefix_and_line_number))

        return combinations

